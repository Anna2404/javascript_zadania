//events - zdarzenia
//addEventListener
//removeEventListener
//attachEvent- dołącz eventy
//detachEvent

function createEvent(obj, eventName, functionToInvoke) {
	if (document.addEventListener)
		obj.addEventListener(eventName, functionToInvoke);
	else
		obj.attachEvent("on" + eventName, functionToInvoke);
}

function zmienKolor() {
	this.className = "zmienKolor";
}

function zmienKolor2() {
	this.remobeAttribute("class");
}

function powiekszCzcionke() {
	var fontSize = parseInt(window.getComputedStyle(this).fontSize);
	this.style.fontSize = (++fontSize) + "px";
	//	this.style.fontSize = "20px";
}
window.onload = function () {
	var test = document.getElementById("test");
	var stop = document.getElementById("stop");
	createEvent(test, "mouseover", zmienKolor);
	createEvent(test, "mouseover", powiekszCzcionke);
	createEvent(test, "mouseout", zmienKolor2);
	//	test.onmouseover = zmienKolor;
	//	test.onmouseout = zmienKolor2;
	//	test.addEventListener("mouseover", zmienKolor);
	//	test.addEventListener("mouseout", zmienKolor2);
	//	test.addEventListener("mouseout", powiekszCzcionke);
		stop.addEventListener("click", function () {
			test.removeEventListener("mouseover", powiekszCzcionke);
		});
}
