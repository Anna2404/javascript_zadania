//onmouseover- gdy kursor myszy najedzie na Element
//onmouseout- gdy kursor myszy opuści Element
//onmousemove- gdy kursor myszy "jeździ" po elemencie
//onclick-gdy element zostanie kliknięty
//ondblclick-gdy element zostanie 2X szybko kliknięty
//onclick to tak naprawdę:
//onmousedown- gdy mamy wciśnięty przycisk myszy
//onmouseup-gdy opuścimy przycisk myszy 

function movingImage(e, objToMove) {
	objToMove.style.left = e.clientX - objToMove.width / 2 + "px";
	objToMove.style.top = e.clientY - objToMove.height / 2 + "px";
}
window.onload = function () {
	var buzka = document.getElementById("buzka");
	buzka.onmousedown = function () {
		var self = this;
		document.onmousemove = function (e) {

			movingImage(e, self);
		};
	};
	buzka.onmouseup = function () {
		document.onmousemove = null;
	};

	buzka.ondragstart = function (e) {
		e.preventDefault();
	}
};
