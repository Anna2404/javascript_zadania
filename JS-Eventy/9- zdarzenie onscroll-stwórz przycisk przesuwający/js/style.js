//window.pageYOffset;
//window.scrollBy(xaxis,yaxis);

window.onload = function () {
	var toTopButton = document.getElementById("toTopButton");
	var test = document.getElementById("test");
	window.onscroll = function () {
		var test = document.getElementById("test");
		//		poziome skrolowanie
		var yScrollAxis = window.pageYOffset;
		if (yScrollAxis > 300)
			test.innerHTML = yScrollAxis;
		//		aby przycisk to top pojawił się później przy skrolowaniu (nie od razu jak jesteśmy na poczatku stronu)należy wpisać:
		toTopButton.style.display = "block";

		else
			toTopButton.style.display = "none";

	}
	toTopButton.onclick = function () {
		window.scrollBy(0, -1 * window.pageYOffset);
	};
};
