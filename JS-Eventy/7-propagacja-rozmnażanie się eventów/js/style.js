//propagacja- rozmnażanie 

function wykonaj(event, eventObj) {
	//	wspiera aby był zawsze dostęp eventu przez wszystkie przeglądarki 
	var e = event || window.event;
	//	wspiera dostęp eventu do źródła
	var srcElement = e.target || e.srcElement;
	//	pobranie divu tmp
	var tmp = document.getElementById("tmp");
	//	odwołanie się do źródła w którym zostało wywołane zdarzenie
	tmp.innerHTML = "źródło eventu" + srcElement.tagName + "<br>event przypisany do tagu:" + eventObj.tagName;
}


window.onload = function () {
	var test = document.getElementById("test");
	var pogrubiony = document.getElementById("pogrubiony");
	var przycisk = document.getElementById("przycisk");
	//	test.onmousemove = function (event)
	//		(wykonaj(event, this);
	//
	//		);
	//};
	//inny wariant 
	test.onclick = function (event) {
		wykonaj(event, this);

	};

	test.onclick = function (event) {
		alert("test");
		wykonaj(event, this);

	};

	pogrubiony.onclick = function (event) {
		alert("pogrubiony");

	};

	przycisk.onclick = function (event) {
		var e = event || window.event;
		//	aby było wspierane przez inne wersje internet explorer należy dodać:
		if (e.stopPropagation)
			e.stopPropagation();
		else
			e.canceBubble = true;
		alert("przycisk");
		event.stopPropagation();
	};
};
