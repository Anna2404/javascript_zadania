person = {
	imie: "Arek",
	nazwisko: "Kowalski",
	age: 34
};

var kursyProgramowania = document.getElementById("kursyProgramowania").getElementsByTagName("li");

kursy = [
	"Java",
	"C++",
	"PHP"
];
//alert(person["imie"]);

//for (var property in kursy) {
//	alert(kursy[property]);
//}


//inna możliwość

//var wszystkieKursyWStringu = "";
//for (var property in kursy) {
//	wszystkieKursyWStringu += kursy[property];
//}
//alert(wszystkieKursyWStringu);

//jeszcze inny wariant

for (var property in kursyProgramowania) {
	if (typeof (kursyProgramowania[property]) !== "object")
		break;
	alert(kursyProgramowania[property].innerHTML);
}
