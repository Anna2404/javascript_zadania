/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var kursyProgramowania = ['C++', 'Java', 'C#', 'Pascal'];


var rezultat = document.getElementById('rezultat');

//while (i < 5)
//	(
//		rezultat.innerHTML += kursyProgramowania[0]; i++;
//	)


//rezultat.innerHTML += kursyProgramowania[0];
//rezultat.innerHTML += kursyProgramowania[1];
//rezultat.innerHTML += kursyProgramowania[2];
//rezultat.innerHTML += kursyProgramowania[3];

//Jak zrobić to samo ale krócej?, aby nie trzeba było za każdym razem wypisywać rezultat.innerHTML += kursyProgramowania[0];należy użyć while- gdy czyli podczas gdy 

//var i = 0;
//
//while (i < 5) {
//	alert(i);
//	//		i++ - inkrementacja - należy dodać, aby za każdym razem dodawało 1, aby wykonywana funkcja po każdym okrążeniu zwiększała się o 1 a nie w kółko wykonywała to samo
//	i++;
//}

//inaczej można też zapisać:

//var i = 0;
//
//
//while (i < kursyProgramowania.length) {
//	rezultat.innerHTML += kursyProgramowania[i] + "<br>";
//
//	i++;
//}


//inny wariant pętli z dodaniem jakiegoś słowa np "test" wówczas przy dopisywaniu w html lowej pozycji w li zostanie dodany podany przez nas wyraz za każdym razem do nowej pozycji, nie trzeba dodatkowo dodawać czyli automatycznie zostaje dodana treść można również dodać kolejne warianty czyli jeżeli jest równy c++ to dodaj treść HIT a w innym wypadku PROMOCJA

var kursyProgramowania = document.getElementById('kursyProgramowania').getElementsByTagName('li');
var i = 0;
while (i < kursyProgramowania.length) {
	if (kursyProgramowania[i].innerHTML === "C++")
		kursyProgramowania[i].innerHTML += "HIT";
	else
		kursyProgramowania[i].innerHTML += "PROMOCJA";
	i++;
}


//rób dopóki  nie zostanie spełniont warunek

//do{
//	alert(i);
//	i++;
//}while (i<5);
