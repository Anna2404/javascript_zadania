$("document").ready(
	function () {
		//przypisanie nagłówka do odpowiedniego tekstu. 
		//		wybieranie wszystkich divów o nazwie nagłowek i wchodzimy w span. Wybieramy funkcję each- pętlowa. Dla każdego elemantu ma się wykonywać pewnego rodzaju funkcje
		$("div.naglowek span").each(
			//chcemy aby do każdego została przypisana jakaś klasa do każdego spana 
			function (i) {
				//				więc wybieramy ten który został aktualnie klinkięty czyli this następnie dodajemy klasę np.o nazwie gh
				$(this).addClass("gh" + i);
			}
		);

		//		tak samo dla ukryty tekst:
		$("div.ukryty_tekst").each(
			function (i) {
				$(this).addClass("gh" + i);
			}
		);

		//		Jeżeli ktoś kliknie
		$("div.naglowek span").click(
			//			kiedy wybierzemy this jakąś ikonę zostanie automatycznie pobrany atrybut class
			//			1000 powoduje animację tekstu powolne pojawianie się tekstu z lewej strony inne wartości - slow, normal, fast 
			function () {
				$("div." + $(this).attr("class")).toggle(1000);
			}
			//			zostanie wywołana funckcja przełączenia toggle- przełączenie się między jakimiś stanami. Aktualny stan tekstu jest display: none czyli niewidoczny.
			//zmienia tekst schowaj na więcej i odwrotnie. Przełączanie funckji 
		).toggle(
			function () {
				//				ten tekst zmień na schowaj
				$(this).text("schowaj");
			},
			//			w innym wypadku na więcej 
			function () {
				$(this).text("więcej");
			}

		);

	}
);
